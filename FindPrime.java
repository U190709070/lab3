public class FindPrime {

    public static void main(String[] args) {

        int number = Integer.parseInt(args[0]);

        for (int num = 0; num <= number; num++) {
            int x = 0;
            int i = 2;
            while (i < num){
                if (num % i == 0){
                    x = 1;
                    break;
                }
                i++;
            }
            if (num == 0 || num ==1){
                x = 0;
            }else if (x==0){
                System.out.print(num+" ");
            }


        }
    }
}